#ifndef TES_EXAMPLE_H_
#define TES_EXAMPLE_H_
#include "Eigen/Core"
#include "Eigen/Dense"
#include <vector>

using namespace Eigen;

typedef Matrix<double, 3, 1> Vector3d;
typedef Matrix<double, 6, 1> Vector6d;
typedef Matrix<double, 6, 6> Matrix6d;

class Example {
private:
  int num_dim;
public:
  void set_num_dim(int input_value);
  int get_num_dim();
  Matrix3d get_identity ();
  void print_matrix (Matrix3d mat);
  void set_zero (MatrixXd& mat);
  MatrixXd matrix_add_eye(MatrixXd& mat);
  MatrixXd matrix_slicing(MatrixXd mat);
  void householderQr_solve(MatrixXd& mat_a, MatrixXd& mat_b);
  void colPivHouseholderQr_solve(MatrixXd& mat_a, MatrixXd& mat_b);
  void fullPivHouseholderQr_solve(MatrixXd& mat_a, MatrixXd& mat_b);
  void ldlt_solve(MatrixXd& mat_a, MatrixXd& mat_b);
  void linear_solver_comparison(MatrixXd& mat_a, MatrixXd& mat_b);
};

#endif  // TES_EXAMPLE_H_
