#include "example.h"
#include <iostream>
#define PRINT_MAT(X) cout << #X << ":\n" << X << endl << endl

using namespace std;

void Example::set_num_dim(int input_value) {
  Example::num_dim = input_value;
}

int Example::get_num_dim() {
  return Example::num_dim;
}

void Example::print_matrix(Matrix3d mat) {
  cout << mat << endl;
}

Matrix3d Example::get_identity() {
  Matrix3d result = Matrix3d::Identity();
  return result;
}

void Example::set_zero (MatrixXd& mat) {
  mat = MatrixXd::Zero(mat.rows(), mat.cols());
}

MatrixXd Example::matrix_add_eye(MatrixXd& mat) {
  MatrixXd eye = MatrixXd::Identity(mat.rows(), mat.cols());
  mat += eye;
  return mat;
}

MatrixXd Example::matrix_slicing(MatrixXd mat) {
  if (mat.rows() > mat.cols()) {
    return mat.block(0 , 0, mat.cols(), mat.cols());
  } else if (mat.rows() < mat.cols()) {
    return mat.block(0 , 0, mat.rows(), mat.rows());
  } else {
    return mat;
  }
}

void Example::householderQr_solve(MatrixXd& mat_a, MatrixXd& mat_b) {
  MatrixXd x;
  clock_t begin;
  clock_t end;
  double elapsed_secs;

  cout << "HouseholderQR"  << endl;
  begin = clock();
  x = mat_a.householderQr().solve(mat_b);
  end = clock();
  elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
  cout << "Time taken    : " << elapsed_secs << endl;
  cout << "L2 norm Error : " << (mat_a * x - mat_b).norm() << endl;
}

void Example::colPivHouseholderQr_solve(MatrixXd& mat_a, MatrixXd& mat_b) {
  MatrixXd x;
  clock_t begin;
  clock_t end;
  double elapsed_secs;

  cout << "ColPivHouseholderQR"  << endl;
  begin = clock();
  x = mat_a.colPivHouseholderQr().solve(mat_b);
  end = clock();
  elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
  cout << "Time taken    : " << elapsed_secs << endl;
  cout << "L2 norm Error : " << (mat_a * x - mat_b).norm() << endl;
}

void Example::fullPivHouseholderQr_solve(MatrixXd& mat_a, MatrixXd& mat_b) {
  MatrixXd x;
  clock_t begin;
  clock_t end;
  double elapsed_secs;

  cout << "FullPivHouseholderQR" << endl;
  begin = clock();
  x = mat_a.fullPivHouseholderQr().solve(mat_b);
  end = clock();
  elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
  cout << "Time taken    : " << elapsed_secs << endl;
  cout << "L2 norm Error : " << (mat_a * x - mat_b).norm() << endl;
}

void Example::ldlt_solve(MatrixXd& mat_a, MatrixXd& mat_b) {
  MatrixXd x;
  clock_t begin;
  clock_t end;
  double elapsed_secs;

  cout << "LDLT" << endl;
  begin = clock();
  x = mat_a.ldlt().solve(mat_b);
  end = clock();
  elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
  cout << "Time taken    : " << elapsed_secs << endl;
  cout << "L2 norm Error : " << (mat_a * x - mat_b).norm() << endl;
}

void Example::linear_solver_comparison(MatrixXd& mat_a, MatrixXd& mat_b) {
  // Comparison of simultaneous equation solver.
  // The problem: You have a system of equations, that you have written as
  // a single matrix equation
  // Ax = b
  // Where A and b are matrices (b could be a vector, as a special case).
  // You want to find a solution x.
  // see also http://eigen.tuxfamily.org/dox/group__TutorialLinearAlgebra.html

  cout << "Solve x: A * x = b *************************************" << endl;
  cout << "A is " << mat_a.rows() << "x" << mat_a.cols() << " matrix" << endl;
  cout << "b is " << mat_b.rows() << "x" << mat_b.cols() << " matrix" << endl;
  Example::householderQr_solve(mat_a, mat_b);
  cout << endl;
  Example::colPivHouseholderQr_solve(mat_a, mat_b);
  cout << endl;
  Example::fullPivHouseholderQr_solve(mat_a, mat_b);
  cout << endl;
  Example::ldlt_solve(mat_a, mat_b);
  cout << endl;
}
