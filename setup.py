from distutils.core import setup, Extension
import numpy

example_module = Extension('_example',
                           sources=['example_wrap.cxx', 'example.cpp'],
                           include_dirs=[numpy.get_include()])

setup (name = 'example',
       version = '0.1',
       author      = "SWIG Docs",
       description = """Simple swig example""",
       ext_modules = [example_module],
       py_modules = ["example"],
       )
