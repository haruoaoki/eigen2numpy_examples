from example import Example
import numpy as np
import time

print('Generate Example instance')
e = Example()

print('Set private class member.  Example::num_dim')
n = 5
e.set_num_dim(n)

print('Get private class member.  Example::num_dim')
assert n == e.get_num_dim(), "Error: "

print ('Get 3d matrix.  Example::get_identity')
m = e.get_identity()

print ('Print 3d matrix.  Example::print_matrix')
e.print_matrix(m)

print ('Set zero matrix passing by reference')
a = np.ones((5, 3))
print('a: Before Example::set_zero(a)')
print(a)
e.set_zero(a)
print('a: After Example::set_zero(a)')
print(a)

print ('Add identity matrix.  Example::matrix_add_eye')
b = np.zeros((4, 4))
print('Original matrix')
c = e.matrix_add_eye(b)
print('Added indentity matrix')
print(c)

print('Example::matrix_slicing')
a = np.random.random((5, 3))
print('Original matrix')
print(a)
print('Sliced matrix')
print(e.matrix_slicing(a))

print('Testing Example::linear_solver_comparison')
a = np.array([[1., 2., 3.],
              [4., 5., 6.],
              [7., 8., 10.]])
b = np.array([3., 3., 4.])
print('Matrix a')
print(a)
print('Matrix b')
print(b)
print('x')
print(np.linalg.solve(a, b))
e.linear_solver_comparison(a, b)

a = np.random.random((1000, 1000))
b = np.random.random(1000)
e.linear_solver_comparison(a, b)
start = time.time()
x = np.linalg.solve(a, b)
elapsed_time = time.time() - start
print("Python Numpy: numpy.linalg.solve")
print("elapsed_time:{0}".format(elapsed_time))
print('L2 norm error: ' + str(np.linalg.norm(np.dot(a, x) - b)))
