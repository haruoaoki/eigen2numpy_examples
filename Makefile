all: example_wrap.cxx
	CC=g++ CXX=g++ CFLAGS="-O3" python setup.py build_ext --inplace
	python testexample.py

example_wrap.cxx: example.i example.cpp example.h eigen.i numpy.i
	swig -c++ -python example.i

clean:
	rm -f example.cxx example_wrap.cxx example.py example.pyc _example* eigenpy.py _eigenpy*
	rm -f -r build
