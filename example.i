/* -*- C -*-  (not really, but good for syntax highlighting) */
/* File: example.i */
%module example

%{
//#define SWIG_FILE_WITH_INIT
#include "example.h"
%}
%include <std_vector.i>
%include <eigen.i>
%include <typemaps.i>

%init %{
import_array();
%}

%eigen_typemaps(MatrixXd)
%eigen_typemaps(Matrix3d)
%eigen_typemaps(VectorXd)
%eigen_typemaps(Vector3d)

namespace std {
   %template(vectord) vector<double>;
};

namespace Eigen{};

%include "example.h"
