# eigen2numpy_swig #
The Example of Eigen (C++) to Numpy (Python) Interfacing using SWIG.

### Requirements ###
* SWIG 3.0.3 or later.
* Python 2.7.* and Numpy.
* Eigen 3.2.3 or later.